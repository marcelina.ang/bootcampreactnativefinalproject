import firebase from "firebase";

export function registerUser(data) {
    console.log("data untuk registrasi: ", { data });
    const db = firebase
        .firestore()
        .collection("users")
        .doc(data.email)
        .set(data)
        .then(() => {
            console.log("Document written");
        })
        .catch((error) => {
            console.error("Error adding document: ", error);
        });
    return () => db();
}

export function loginUser(data) {
    const db = firebase
        .firestore()
        .collection("users")
        .doc(data.email)
        .update({
            lastLoginDate: data.lastLogin,
        })
        .then(() => {
            console.log("Document successfully updated!");
        })
        .catch((error) => {
            console.error("Error updating document: ", error);
        });
    return () => db();
}

const purchaseMaterial = ({ materialData }) => {
    let tempMaterial = {};
    const response = firebase
        .firestore()
        .collection("material")
        .where("idKelas", "==", materialData.idKelas)
        .orderBy("sort", "asc")
        .get()
        .then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                tempMaterial = {
                    idMaterial: doc.id,
                    idKelas: doc.data().idKelas,
                    title: doc.data().title,
                    image: doc.data().image,
                    description: doc.data().description,
                    materialProgress: "Belum dimulai",
                    sort: doc.data().sort,
                    email: materialData.email
                };
                console.log(tempMaterial);
                firebase
                    .firestore()
                    .collection("materialPurchase")
                    .add(tempMaterial)
                    .then(() => {
                        console.log("Document written");
                    })
                    .catch((error) => {
                        console.error("Error adding document: ", error);
                    });
            });
        });
};

export function purchaseClass(data) {
    const db = firebase
        .firestore()
        .collection("purchase")
        .add(data)
        .then(() => {
            console.log("Document written");
        })
        .catch((error) => {
            console.error("Error adding document: ", error);
        });
    const materialData = { email: data.email, idKelas: data.idKelas };
    purchaseMaterial({ materialData });

    return () => db();
}
