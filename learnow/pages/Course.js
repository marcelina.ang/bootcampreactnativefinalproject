import React, { useEffect, useState } from "react";
import { FlatList, Image, Text, View, StyleSheet } from "react-native";
import firebase from "firebase";
import {
    darkTextColor,
    lightTextColor,
    primary1Color,
    primary6Color,
} from "../constant/color";
import {
    DarkLoadingTransparent,
    DarkNoDataTransparent,
} from "../components/Placeholder";

const MaterialItem = ({ title, description, materialProgress, image }) => (
    <View style={styles.materialContainer}>
        <View style={styles.materialImageContainer}>
            <Image
                style={styles.materialImage}
                source={{ uri: image }}
                resizeMode="contain"
            />
        </View>
        <View style={styles.materialTextContainer}>
            <Text style={styles.materialName}>{title}</Text>
            <Text style={styles.materialDescription}>{description}</Text>
            <Text style={styles.materialProgress}>{materialProgress}</Text>
        </View>
    </View>
);

const MaterialComponent = ({
    loadingMaterial,
    material,
    renderMaterialItem,
}) => {
    if (loadingMaterial) {
        return <DarkLoadingTransparent />;
    } else if (material.length == 0) {
        return <DarkNoDataTransparent />;
    } else {
        return (
            <FlatList
                style={styles.materialListContainer}
                data={material}
                renderItem={renderMaterialItem}
                keyExtractor={(item) => item.id}
            />
        );
    }
};

export default function Course({ route }) {
    const { idKelas, categoryName, titleKelas, progress, imageKelas, email } =
        route.params;
    const [material, setMaterial] = useState([]);
    const [loadingMaterial, setLoadingMaterial] = useState(true);

    const renderMaterialItem = ({ item }) => (
        <MaterialItem
            title={item.title}
            description={item.description}
            materialProgress={item.materialProgress}
            image={item.image}
        />
    );
    const fetchMaterial = async () => {
        try {
            setLoadingMaterial(true);
            let tempMaterial = [];
            console.log(email, idKelas);
            const response = await firebase
                .firestore()
                .collection("materialPurchase")
                .where("idKelas", "==", idKelas)
                .where("email", "==", email)
                .orderBy("sort", "asc")
                .get();

            response.forEach((doc) => {
                console.log("kuy get course");
                console.log(doc.data());

                tempMaterial.push({
                    id: doc.id,
                    title: doc.data().title,
                    image:
                        doc.data().image == "" ? imageKelas : doc.data().image,
                    description: doc.data().description,
                    materialProgress: doc.data().materialProgress,
                });
            });

            setMaterial(tempMaterial);
            setLoadingMaterial(false);
        } catch (err) {
            console.error(err);
            setLoadingMaterial(false);
        }
    };

    useEffect(() => {
        fetchMaterial();
        return () => {
            console.log("clean up course detail");
        };
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.headerContainer}>
                <Text style={styles.title}>Kelas</Text>
                <View style={styles.classContainer}>
                    <View style={styles.classImageContainer}>
                        <Image
                            style={styles.classImage}
                            source={{ uri: imageKelas }}
                            resizeMode="contain"
                        />
                    </View>
                    <View style={styles.classTextContainer}>
                        <Text style={styles.className}>{titleKelas}</Text>
                        <Text style={styles.classCategory}>{categoryName}</Text>
                        <Text style={styles.classProgress}>{progress}</Text>
                    </View>
                </View>
            </View>
            <View style={styles.materialSectionContainer}>
                <View style={styles.lightHeaderContainer}>
                    <Text style={styles.darkTitle}>Materi</Text>
                </View>
                <MaterialComponent
                    loadingMaterial={loadingMaterial}
                    renderMaterialItem={renderMaterialItem}
                    material={material}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: primary1Color,
        justifyContent: "center",
        alignItems: "center",
    },

    headerContainer: {
        paddingHorizontal: 30,
        justifyContent: "center",
        backgroundColor: primary1Color,
        alignSelf: "stretch",
        paddingVertical: 20,
    },
    materialSectionContainer: {
        flex: 5,
        alignSelf: "stretch",
        backgroundColor: primary6Color,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
        marginTop: 20,
    },

    classContainer: {
        flexDirection: "row",
        justifyContent: "flex-start",
    },
    classImageContainer: {
        width: 100,
        marginLeft: -5,
    },
    classTextContainer: {
        paddingLeft: 10,
        paddingVertical: 5,
        flex: 1,
        justifyContent: "space-evenly",
    },
    classImage: {
        width: 90,
        height: 90,
        alignSelf: "center",
    },
    className: {
        fontWeight: "700",
        fontSize: 14,
        color: lightTextColor,
    },
    classCategory: {
        fontSize: 12,
        color: lightTextColor,
    },
    classProgress: {
        fontWeight: "700",
        fontSize: 12,
        color: lightTextColor,
    },

    materialListContainer: {
        marginHorizontal: 18,
    },
    materialContainer: {
        margin: 10,
        padding: 5,
        borderRadius: 10,
        backgroundColor: lightTextColor,
        flex: 1,
        flexDirection: "row",
        elevation: 2
    },
    materialImageContainer: {
        borderRadius: 10,
        marginLeft: 5,
        flex: 1,
    },
    materialTextContainer: {
        marginBottom: 5,
        padding: 10,
        flex: 3.5,
        justifyContent: "space-evenly",
    },
    materialImage: {
        width: "100%",
        height: 90,
        marginVertical: 3
    },
    materialName: {
        fontWeight: "700",
        fontSize: 14,
        color: darkTextColor,
    },
    materialDescription: {
        fontSize: 12,
        color: darkTextColor,
    },
    materialProgress: {
        fontWeight: "700",
        fontSize: 12,
        color: darkTextColor,
    },
    title: {
        marginBottom: 15,
        fontWeight: "700",
        fontSize: 26,
        color: lightTextColor,
        alignSelf: "flex-start",
    },

    lightHeaderContainer: {
        justifyContent: "flex-end",
        alignSelf: "stretch",
        paddingHorizontal: 30,
        paddingTop: 30,
    },
    darkTitle: {
        marginVertical: 5,
        fontWeight: "700",
        fontSize: 26,
        color: darkTextColor,
    },
});
