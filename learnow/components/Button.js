import React from "react";
import { Text, TouchableOpacity, StyleSheet, View } from "react-native";
import { accent2Color, disabledColor, disabledTextColor, lightTextColor, primary1Color, primary2Color } from "../constant/color";

const Button = ({ text, buttonStyle, buttonTextStyle, onPress }) => (
    <View style={styles.buttonContainer}>
        <TouchableOpacity
            style={buttonStyle}
            onPress={onPress}
            activeOpacity={0.7}
        >
            <Text style={buttonTextStyle}>{text}</Text>
        </TouchableOpacity>
    </View>
);

export const PrimaryButton = ({ text, onPress }) => {
    return (
        <Button
            text={text}
            onPress={onPress}
            buttonStyle={styles.primaryButton}
            buttonTextStyle={styles.primaryButtonText}
        />
    );
};

export const OutlineButton = ({ text, onPress }) => {
    return (
        <Button
            text={text}
            onPress={onPress}
            buttonStyle={styles.outlineButton}
            buttonTextStyle={styles.outlineButtonText}
        />
    );
};

export const SmallPrimaryButton = ({ text, onPress }) => {
    return (
        <Button
            text={text}
            onPress={onPress}
            buttonStyle={styles.smallPrimaryButton}
            buttonTextStyle={styles.smallPrimaryButtonText}
        />
    );
};

export const SmallDisabledButton = ({ text }) => {
    return (
        <View style={styles.buttonContainer}>
            <View style={styles.smallDisabledButton}>
                <Text style={styles.smallDisabledButtonText}>{text}</Text>
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: "row",
        marginVertical: 5,
    },

    primaryButton: {
        marginVertical: 5,
        borderWidth: 1,
        borderRadius: 22.5,
        height: 45,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: primary1Color,
        borderColor: primary1Color,
        elevation: 4,
    },
    primaryButtonText: {
        color: lightTextColor,
        fontSize: 18,
    },

    outlineButton: {
        marginVertical: 5,
        // borderWidth: 1,
        borderRadius: 22.5,
        height: 45,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // borderColor: primary2Color,
        backgroundColor: lightTextColor,
        elevation: 4,
    },
    outlineButtonText: {
        color: primary2Color,
        fontSize: 18,
    },

    smallPrimaryButton: {
        marginTop: 5,
        borderWidth: 1,
        borderRadius: 15,
        marginHorizontal: 25,
        height: 30,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: accent2Color,
        borderColor: accent2Color,
        elevation: 4,
    },
    smallPrimaryButtonText: {
        color: lightTextColor,
        fontSize: 14,
    },

    smallDisabledButton: {
        marginTop: 5,
        borderWidth: 1,
        borderRadius: 15,
        marginHorizontal: 25,
        height: 30,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: disabledColor,
        borderColor: disabledColor,
    },
    smallDisabledButtonText: {
        color: disabledTextColor,
        fontSize: 14,
    },
});
