import React from "react";
import { ActivityIndicator, Image, StyleSheet, Text, View } from "react-native";
import {
    darkTextColor,
    lightTextColor,
    primary1Color,
} from "../constant/color";

export const DarkNoDataTransparent = () => {
    return (
        <View style={styles.containerTransparent}>
            <Image
                style={styles.onboardingImage}
                source={require("../assets/onboarding.jpg")}
                resizeMode="contain"
            />
            <Text style={styles.noDataDarkText}>Tidak ada data</Text>
        </View>
    );
};

export const DarkLoadingTransparent = () => {
    return (
        <View style={styles.containerTransparent}>
            <View style={styles.row}>
                <Image
                    style={styles.logo}
                    source={require("../assets/small-logo-dark.png")}
                    resizeMode="contain"
                />
                <ActivityIndicator size="small" color={darkTextColor} />
            </View>
        </View>
    );
};

export const LightLoadingTransparent = () => {
    return (
        <View style={styles.containerTransparent}>
            <View style={styles.row}>
                <Image
                    style={styles.logo}
                    source={require("../assets/small-logo.png")}
                    resizeMode="contain"
                />
                <ActivityIndicator size="small" color={lightTextColor} />
            </View>
        </View>
    );
};

export const LightLoadingScreen = () => {
    return (
        <View style={styles.containerPrimary1Color}>
            <View style={styles.row}>
                <Image
                    style={styles.logo}
                    source={require("../assets/small-logo.png")}
                    resizeMode="contain"
                />
                <ActivityIndicator size="small" color={lightTextColor} />
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    containerTransparent: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    containerPrimary1Color: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: primary1Color,
        paddingBottom: "15%",
    },
    row: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    logo: {
        alignSelf: "center",
        marginRight: 10,
    },
    
    onboardingImage: {
        width: "35%",
        height: "35%"
    },

    noDataDarkText: {
        fontSize: 16,
        color: darkTextColor,
        textAlign: "center",
    },
});
