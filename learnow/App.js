import { StatusBar } from "expo-status-bar";
import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import Router from "./router";
import firebase from "firebase/app";

const firebaseConfig = {
    apiKey: "AIzaSyAx7_t_YYVRxGVoDu8IHPrVnq9PbQ4g0HQ",
    authDomain: "learnow-515a2.firebaseapp.com",
    projectId: "learnow-515a2",
    storageBucket: "learnow-515a2.appspot.com",
    messagingSenderId: "340482622923",
    appId: "1:340482622923:web:80cf056035d1b55934e24e",
};

let app;

if (firebase.apps.length === 0) {
    app = firebase.initializeApp(firebaseConfig);
} else {
    app = firebase.app;
}

export default function App() {
    return (
        <SafeAreaView style={styles.container}>
            <Router />
            <StatusBar style="auto" />
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
