export const primary1Color = "#2b468b"
export const primary2Color = "#699eee"
export const primary3Color = "#c0d8fc"
export const primary4Color = "#cce1fe"
export const primary5Color = "#e0ebfd"
export const primary6Color = "#eaf3fa"

export const accent1Color = "#ed7d2b"
export const accent2Color = "#fa9746"
export const accent3Color = "#efa170"
export const accent4Color = "#ffb27e"

export const darkTextColor = "#2b468b"
export const lightTextColor = "#FFFFFF"

export const disabledTextColor = "#ECECEC" 
export const disabledColor = "#CECECE"
