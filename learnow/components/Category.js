import React from "react";
import {
    FlatList,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from "react-native";
import { Ionicons } from "@expo/vector-icons";
import {
    accent2Color,
    lightTextColor,
    primary1Color,
} from "../constant/color";

const CategoryItem = ({ title, icon, id, filter, setFilter, fetchKelas }) => {
    return (
        <TouchableOpacity
            style={styles.categoryContainer}
            onPress={() => {
                if (filter != id) {
                    setFilter(id);
                    fetchKelas(id);
                } else {
                    setFilter("");
                    fetchKelas("");
                }
            }}
        >
            <View
                style={
                    filter == id
                        ? styles.categorySelectedImage
                        : styles.categoryImage
                }
            >
                <Ionicons name={icon} size={24} color={primary1Color} />
            </View>
            <Text
                style={
                    filter == id
                        ? styles.categorySelectedName
                        : styles.categoryName
                }
            >
                {title}
            </Text>
        </TouchableOpacity>
    );
};

export function Category({ kategori, fetchKelas, filter, setFilter }) {
    const renderCategoryItem = ({ item }) => (
        <CategoryItem
            title={item.title}
            icon={item.icon}
            id={item.id}
            filter={filter}
            setFilter={setFilter}
            fetchKelas={fetchKelas}
            filter={filter}
            setFilter={setFilter}
        />
    );

    return (
        <View style={styles.categorySectionContainer}>
            <Text style={styles.title}>Kategori</Text>
            <FlatList
                style={styles.categoryListContainer}
                data={kategori}
                renderItem={renderCategoryItem}
                keyExtractor={(item) => item.id}
                horizontal={true}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    categorySectionContainer: {
        justifyContent: "center",
        alignItems: "center",
        height: 160,
        borderRadius: 20,
        marginHorizontal: 20,
        paddingHorizontal: 10,
    },
    categoryListContainer: {
        flex: 1,
    },
    categoryContainer: {
        marginRight: 10,
        justifyContent: "center",
        alignItems: "center",
        width: 80,
    },
    categoryImage: {
        width: 80,
        height: 80,
        backgroundColor: lightTextColor,
        borderRadius: 80 / 2,
        justifyContent: "center",
        alignItems: "center",
    },
    categorySelectedImage: {
        width: 80,
        height: 80,
        backgroundColor: accent2Color,
        borderRadius: 80 / 2,
        justifyContent: "center",
        alignItems: "center",
    },
    categoryName: {
        color: lightTextColor,
        textAlign: "center",
    },
    categorySelectedName: {
        color: accent2Color,
        textAlign: "center",
    },
    title: {
        marginBottom: 15,
        fontWeight: "700",
        fontSize: 26,
        color: lightTextColor,
        alignSelf: "flex-start",
    },
});
