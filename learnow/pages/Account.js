import React, { useContext, useEffect, useState } from "react";
import { Image, Text, View, ScrollView, StyleSheet } from "react-native";
import Constants from "expo-constants";
import firebase from "firebase";
import { OutlineButton } from "../components/Button";
import { InputWithLabel } from "../components/Input";
import { AuthContext } from "../context/AuthContext";
import {
    darkTextColor,
    lightTextColor,
    primary1Color,
    primary6Color,
} from "../constant/color";

export default function Account({ navigation }) {
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [joinDate, setJoinDate] = useState();
    const [lastLogin, setLastLogin] = useState();
    const [user, setUser] = useContext(AuthContext);

    const submit = () => {
        const auth = firebase
            .auth()
            .signOut()
            .then(() => {
                setUser("");
                navigation.navigate("Onboarding");
                console.info("signed out");
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                Alert.alert(errorCode.toString(), errorMessage.toString());
            });
        return () => auth();
    };

    useEffect(() => {
        const fetchUser = async () => {
            try {
                const response = await firebase
                    .firestore()
                    .collection("users")
                    .where("email", "==", user)
                    .get();

                response.forEach((doc) => {
                    const joinDateRaw = new Date(doc.data().joinDate.toDate());
                    const lastLoginRaw = new Date(
                        doc.data().lastLoginDate.toDate()
                    );
                    setFullName(doc.data().fullName);
                    setEmail(doc.data().email);
                    setJoinDate(
                        joinDateRaw.getDate().toString() +
                            "/" +
                            (joinDateRaw.getMonth() + 1).toString() +
                            "/" +
                            joinDateRaw.getFullYear().toString()
                    );
                    setLastLogin(
                        lastLoginRaw.getDate().toString() +
                            "/" +
                            (lastLoginRaw.getMonth() + 1).toString() +
                            "/" +
                            lastLoginRaw.getFullYear().toString() +
                            " " +
                            (lastLoginRaw.getHours() < 10
                                ? "0" + lastLoginRaw.getHours().toString()
                                : lastLoginRaw.getHours().toString()) +
                            ":" +
                            (lastLoginRaw.getMinutes() < 10
                                ? "0" + lastLoginRaw.getMinutes().toString()
                                : lastLoginRaw.getMinutes().toString()) +
                            ":" +
                            (lastLoginRaw.getSeconds() < 10
                                ? "0" + lastLoginRaw.getSeconds().toString()
                                : lastLoginRaw.getSeconds().toString())
                    );
                });
            } catch (err) {
                console.error(err);
            }
        };
        fetchUser();
    }, []);

    return (
        <View style={styles.container}>
            <View style={styles.upperContainer}>
                <View style={styles.smallLogoContainer}>
                    <Image
                        style={styles.smallLogo}
                        source={require("../assets/small-logo.png")}
                    />
                </View>
            </View>
            <View style={styles.headerContainer}>
                <Image
                    style={styles.photoImage}
                    source={require("../assets/icon.png")}
                />
                <Text style={styles.title}>{fullName}</Text>
            </View>
            <View style={styles.contentContainer}>
                <InputWithLabel label="Email" value={email} />
                <InputWithLabel label="Terdaftar sejak" value={joinDate} />
                <InputWithLabel label="Terakhir login" value={lastLogin} />
            </View>
            <View style={styles.bottomSection}>
                <Text style={styles.credits}>Dibuat oleh:</Text>
                <Text style={styles.credits}>Marcelina Anggraeni</Text>
            </View>
            <View style={styles.buttonSectionContainer}>
                <OutlineButton text={"Keluar"} onPress={submit} />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: Constants.statusBarHeight,
        flex: 1,
        backgroundColor: primary1Color,
    },
    upperContainer: {
        flexDirection: "row",
        height: 70,
        paddingHorizontal: 30,
        justifyContent: "center",
    },
    smallLogoContainer: {
        flex: 1,
        alignItems: "flex-end",
        justifyContent: "center",
    },
    smallLogo: {
        height: 30,
    },

    headerContainer: {
        flex: 4,
        justifyContent: "center",
        alignItems: "center",
        paddingTop: 30,
    },
    contentContainer: {
        flex: 6,
        alignSelf: "stretch",
        paddingHorizontal: 30,
        backgroundColor: primary6Color,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
        paddingTop: 50,
    },

    buttonSectionContainer: {
        flex: 1,
        alignSelf: "stretch",
        justifyContent: "flex-start",
        paddingHorizontal: 60,
        backgroundColor: primary6Color,
    },

    photoImage: {
        width: 120,
        height: 120,
        borderWidth: 2,
        borderColor: primary6Color,
        borderRadius: 60,
    },
    title: {
        marginVertical: 5,
        fontWeight: "700",
        fontSize: 36,
        color: lightTextColor,
    },
    bottomSection: {
        height: 50,
        justifyContent: "center",
        alignSelf: "stretch",
        alignItems: "center",
        backgroundColor: primary6Color

    },
    credits: {
        color: darkTextColor,
        opacity: 0.5,
    },
});
