import React from "react";
import { Image, Text, View, StyleSheet } from "react-native";
import { PrimaryButton, OutlineButton } from "../components/Button";
import { darkTextColor, primary6Color } from "../constant/color";

export default function Onboarding({ navigation }) {
    return (
        <View style={styles.container}>
            <View style={styles.contentSection}>
                <Image
                    style={styles.onboardingImage}
                    source={require("../assets/onboarding.jpg")} // Technology vector created by pch.vector - www.freepik.com
                    resizeMode="contain"
                />
            </View>
            <View style={styles.titleSection}>
                <Text style={styles.title}>Selamat datang!</Text>
                <Text style={styles.subtitle}>
                    Masuk atau buat akun baru untuk memulai
                </Text>
            </View>
            <View style={styles.buttonSection}>
                <PrimaryButton
                    text="Masuk"
                    onPress={() => {
                        navigation.navigate("Login");
                    }}
                />
                <OutlineButton
                    text="Daftar"
                    onPress={() => {
                        navigation.navigate("Register");
                    }}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: primary6Color,
    },
    contentSection: {
        flex: 4,
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        flexDirection: "row",
    },
    titleSection: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 60,
    },
    buttonSection: {
        flex: 1.5,
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 90,
    },

    title: {
        marginVertical: 5,
        fontWeight: "700",
        fontSize: 20,
        color: darkTextColor,
    },
    subtitle: {
        marginTop: 5,
        fontSize: 14,
        color: darkTextColor,
    },

    onboardingImage: {
        marginTop: 100,
        flex: 0.8,
    },
});
