import React, { useCallback, useContext, useEffect, useState } from "react";
import Constants from "expo-constants";
import {
    FlatList,
    Image,
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
} from "react-native";
import firebase from "firebase";
import { SmallDisabledButton, SmallPrimaryButton } from "../components/Button";
import { Category } from "../components/Category";
import { purchaseClass } from "../data/firestoreHandler";
import { useFocusEffect } from "@react-navigation/core";
import { AuthContext } from "../context/AuthContext";
import {
    darkTextColor,
    lightTextColor,
    primary1Color,
    primary6Color,
} from "../constant/color";
import {
    DarkLoadingTransparent,
    DarkNoDataTransparent,
    LightLoadingTransparent,
} from "../components/Placeholder";

const ClassItem = ({
    title,
    categoryName,
    categoryId,
    price,
    navigation,
    image,
    idKelas,
    purchased,
    user,
    fetchPurchased,
}) => {
    let PurchaseButton = ({ purchased }) => {
        console.log(purchased);
        if (purchased.indexOf(idKelas) >= 0) {
            return <SmallDisabledButton text="Sudah dibeli" />;
        } else {
            return (
                <SmallPrimaryButton
                    text="Beli"
                    onPress={() => {
                        const now = new Date();
                        const data = {
                            email: user,
                            idKelas: idKelas,
                            namaKelas: title,
                            categoryId: categoryId,
                            categoryName: categoryName,
                            price: price,
                            image: image,
                            purchaseDate: now,
                            progress: "Belum dimulai",
                            title: title,
                        };
                        fetchPurchased();
                        purchaseClass(data);
                    }}
                />
            );
        }
    };
    return (
        <View style={styles.classContainer}>
            <TouchableOpacity
                onPress={() => {
                    navigation.navigate("CoursePreview", {
                        idKelas: idKelas,
                        imageKelas: image,
                        price: price,
                        categoryName: categoryName,
                        titleKelas: title,
                    });
                }}
            >
                <View style={styles.classImageContainer}>
                    <Image
                        style={styles.classImage}
                        source={{ uri: image }}
                        resizeMode="contain"
                    />
                </View>
                <Text style={styles.className}>{title}</Text>
                <Text style={styles.classCategory}>{categoryName}</Text>
                <Text style={styles.classPrice}>{currencyFormat(price)}</Text>
            </TouchableOpacity>
            <PurchaseButton purchased={purchased} />
        </View>
    );
};

const ClassComponent = ({ loadingKelas, renderClassItem, kelas }) => {
    if (loadingKelas) {
        return <DarkLoadingTransparent />;
    } else if (kelas.length == 0) {
        return <DarkNoDataTransparent />;
    } else {
        return (
            <FlatList
                style={styles.classListContainer}
                data={kelas}
                renderItem={renderClassItem}
                keyExtractor={(item) => item.id}
                numColumns={2}
            />
        );
    }
};

const CategoryComponent = ({
    loadingKategori,
    kategori,
    fetchKelas,
    filter,
    setFilter,
}) => {
    if (loadingKategori) {
        return <LightLoadingTransparent />;
    } else {
        return (
            <Category
                kategori={kategori}
                fetchKelas={fetchKelas}
                filter={filter}
                setFilter={setFilter}
            />
        );
    }
};

const currencyFormat = (num) => {
    return "Rp " + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
};

export default function Home({ navigation }) {
    const [kategori, setKategori] = useState([]);
    const [kelas, setKelas] = useState([]);
    const [purchased, setPurchased] = useState([]);
    const [filter, setFilter] = useState("");
    const [loadingKategori, setLoadingKategori] = useState(true);
    const [loadingKelas, setLoadingKelas] = useState(true);
    const [user, setUser] = useContext(AuthContext);

    const fetchKelas = async (filter) => {
        try {
            setLoadingKelas(true);
            let tempKelas = [];
            let response;
            console.log("received: ", filter);

            if (filter != "") {
                response = await firebase
                    .firestore()
                    .collection("class")
                    .where("categoryId", "==", filter)
                    .orderBy("title", "asc")
                    .get();
            } else {
                response = await firebase
                    .firestore()
                    .collection("class")
                    .orderBy("title", "asc")
                    .get();
            }

            response.forEach((doc) => {
                tempKelas.push({
                    id: doc.id,
                    title: doc.data().title,
                    image: doc.data().image,
                    categoryId: doc.data().categoryId,
                    categoryName: doc.data().categoryName,
                    price: doc.data().price,
                });
            });
            setKelas(tempKelas);
            setLoadingKelas(false);
        } catch (err) {
            console.error(err);
            setLoadingKelas(false);
        }
    };

    const fetchPurchased = async () => {
        try {
            let tempKelas = [];
            console.log("received: ", filter);

            const response = await firebase
                .firestore()
                .collection("purchase")
                .where("email", "==", user)
                .orderBy("purchaseDate", "desc")
                .get();

            response.forEach((doc) => {
                tempKelas.push(doc.data().idKelas);
            });

            console.log("purchased: ", { tempKelas });
            setPurchased(tempKelas);
        } catch (err) {
            console.error(err);
        }
    };

    useEffect(() => {
        const fetchKategori = async () => {
            try {
                setLoadingKategori(true);
                let tempKategori = [];

                const response = await firebase
                    .firestore()
                    .collection("category")
                    .orderBy("sort", "asc")
                    .get();

                response.forEach((doc) => {
                    tempKategori.push({
                        id: doc.id,
                        title: doc.data().title,
                        icon: doc.data().icon,
                    });
                });

                setKategori(tempKategori);
                setLoadingKategori(false);
            } catch (err) {
                console.error(err);
                setLoadingKategori(false);
            }
        };
        fetchKategori();

        fetchPurchased();

        return () => {
            console.log("clean up useEffect of Home Page");
        };
    }, []);

    useFocusEffect(
        useCallback(() => {
            setFilter("");
            fetchKelas("");

            return () => {
                console.log("clean up useFocusEffect of Home Page");
            };
        }, [])
    );

    const renderClassItem = ({ item }) => (
        <ClassItem
            title={item.title}
            categoryName={item.categoryName}
            categoryId={item.categoryId}
            price={item.price}
            image={item.image}
            idKelas={item.id}
            user={user}
            purchased={purchased}
            navigation={navigation}
            fetchPurchased={fetchPurchased}
        />
    );

    return (
        <View style={styles.container}>
            <View style={styles.upperContainer}>
                <View style={styles.smallLogoContainer}>
                    <Image
                        style={styles.smallLogo}
                        source={require("../assets/small-logo.png")}
                        resizeMode="contain"
                    />
                </View>
            </View>
            <CategoryComponent
                loadingKategori={loadingKategori}
                kategori={kategori}
                fetchKelas={fetchKelas}
                filter={filter}
                setFilter={setFilter}
            />
            <View style={styles.classSectionContainer}>
                <View style={styles.headerContainer}>
                    <Text style={styles.title}>Ayo, pilih kelasmu!</Text>
                </View>
                <ClassComponent
                    loadingKelas={loadingKelas}
                    renderClassItem={renderClassItem}
                    kelas={kelas}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: Constants.statusBarHeight,
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: primary1Color,
    },
    upperContainer: {
        flexDirection: "row",
        height: 70,
        paddingHorizontal: 30,
        justifyContent: "center",
    },
    smallLogoContainer: {
        flex: 1,
        alignItems: "flex-end",
        justifyContent: "center",
    },
    smallLogo: {
        height: 30,
    },

    headerContainer: {
        justifyContent: "flex-end",
        alignSelf: "stretch",
        paddingHorizontal: 30,
        paddingTop: 30,
    },

    title: {
        marginVertical: 5,
        fontWeight: "700",
        fontSize: 26,
        color: darkTextColor,
    },

    classSectionContainer: {
        flex: 5,
        alignSelf: "stretch",
        paddingBottom: 10,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
        backgroundColor: primary6Color,
        marginTop: 20,
    },
    classListContainer: {
        marginHorizontal: 18,
    },
    classContainer: {
        margin: 10,
        padding: 5,
        borderRadius: 10,
        backgroundColor: lightTextColor,
        flex: 1,
        elevation: 2,
    },
    classImageContainer: {
        borderRadius: 10,
        marginBottom: 5,
        flex: 1,
    },
    classImage: {
        width: "100%",
        height: 90,
        marginVertical: 3,
    },
    className: {
        fontWeight: "700",
        fontSize: 14,
        color: darkTextColor,
    },
    classCategory: {
        fontSize: 12,
        color: darkTextColor,
    },
    classPrice: {
        fontWeight: "700",
        fontSize: 12,
        color: darkTextColor,
    },
});
