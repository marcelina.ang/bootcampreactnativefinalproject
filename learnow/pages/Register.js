import React, { useContext, useState } from "react";
import {
    Alert,
    Image,
    Text,
    View,
    StyleSheet,
} from "react-native";
import firebase from "firebase";
import { PrimaryButton } from "../components/Button";
import { Input } from "../components/Input";
import { registerUser } from "../data/firestoreHandler";
import { AuthContext } from "../context/AuthContext";
import {
    darkTextColor,
    lightTextColor,
    primary1Color,
    primary6Color,
} from "../constant/color";
import { LightLoadingScreen } from "../components/Placeholder";

export default function Register({ navigation }) {
    const [fullName, setFullName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [user, setUser] = useContext(AuthContext);

    const submit = () => {
        setLoading(true);

        const auth = firebase
            .auth()
            .createUserWithEmailAndPassword(email, password)
            .then((userCredential) => {
                var user = userCredential.user;

                user.updateProfile({
                    displayName: fullName,
                })
                    .then(() => {
                        const now = new Date();
                        const data = {
                            email,
                            fullName,
                            joinDate: now,
                            lastLoginDate: now
                        };
                        registerUser(data);
                        setUser(data.email);
                        navigation.navigate("MainApp");
                        setLoading(false);
                    })
                    .catch((error) => {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        Alert.alert(
                            errorCode.toString(),
                            errorMessage.toString()
                        );
                        setLoading(false);
                    });
                return () => auth();
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode.toString());
                console.log(errorMessage.toString());
                Alert.alert("Coba lagi", "Data belum sesuai, ayo coba lagi!");
                setLoading(false);
            });
    };

    if (loading) {
        return (
            <LightLoadingScreen/>
        );
    } else {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        style={styles.logo}
                        source={require("../assets/white.png")}
                        resizeMode="contain"
                    />
                </View>
                <View style={styles.form}>
                    <Text style={styles.title}>Daftar</Text>
                    <Text style={styles.subtitle}>Ayo bergabung sekarang!</Text>
                    <View style={styles.content}>
                        <Input
                            placeholder="Nama lengkap"
                            value={fullName}
                            onChangeText={(value) => setFullName(value)}
                        />
                        <Input
                            placeholder="Email"
                            autoCompleteType={"email"}
                            value={email}
                            onChangeText={(value) =>
                                setEmail(value.toLowerCase())
                            }
                        />
                        <Input
                            placeholder="Kata sandi"
                            secureTextEntry={true}
                            value={password}
                            onChangeText={(value) => setPassword(value)}
                        />
                        <Input
                            placeholder="Konfirmasi kata sandi"
                            secureTextEntry={true}
                            value={confirmPassword}
                            onChangeText={(value) => setConfirmPassword(value)}
                        />
                    </View>
                    <View style={styles.buttonSection}>
                        <PrimaryButton text="Daftar" onPress={submit} />
                    </View>
                    <View style={styles.bottomSection}>
                        <Text style={styles.credits}>
                            Dibuat oleh:
                        </Text>
                        <Text style={styles.credits}>
                            Marcelina Anggraeni
                        </Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: primary1Color,
    },
    header: {
        flex: 0.5,
        justifyContent: "center",
        alignItems: "center",
        paddingHorizontal: 60,
    },
    form: {
        flex: 1.75,
        justifyContent: "flex-start",
        alignSelf: "stretch",
        paddingHorizontal: 60,
        paddingVertical: 30,
        backgroundColor: primary6Color,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    content: {
        justifyContent: "center",
        alignItems: "center",
    },
    buttonSection: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        marginHorizontal: 30,
        marginTop: 20
    },

    bottomSection: {
        height: 50,
        justifyContent: "center",
        alignSelf: "stretch",
        alignItems: "center",
    },
    logo: {
        height: 70,
        width: 70,
        alignSelf: "center",
    },
    title: {
        marginTop: 10,
        marginBottom: 15,
        fontWeight: "700",
        fontSize: 26,
        color: darkTextColor,
    },
    subtitle: {
        fontSize: 14,
        marginBottom: 20,
        color: darkTextColor,
    },

    appName: {
        marginTop: 10,
        marginBottom: 15,
        fontWeight: "700",
        fontSize: 20,
        color: lightTextColor,
        textAlign: "center",
    },
    credits: {
        color: darkTextColor,
        opacity: 0.5
    },
});
