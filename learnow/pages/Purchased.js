import React, { useCallback, useContext, useEffect, useState } from "react";
import Constants from "expo-constants";
import {
    FlatList,
    Image,
    Text,
    View,
    TouchableOpacity,
    StyleSheet,
} from "react-native";
import firebase from "firebase";
import { Category } from "../components/Category";
import { useFocusEffect } from "@react-navigation/core";
import { AuthContext } from "../context/AuthContext";
import { darkTextColor, lightTextColor, primary1Color, primary6Color } from "../constant/color";
import { DarkLoadingTransparent, DarkNoDataTransparent, LightLoadingTransparent } from "../components/Placeholder";

const ClassItem = ({
    title,
    categoryName,
    progress,
    image,
    navigation,
    email,
    idKelas,
}) => (
    <TouchableOpacity
        style={styles.classContainer}
        onPress={() =>
            navigation.navigate("Course", {
                idKelas: idKelas,
                imageKelas: image,
                progress: progress,
                categoryName: categoryName,
                titleKelas: title,
                email: email,
            })
        }
    >
        <View style={styles.classImageContainer}>
            <Image
                style={styles.classImage}
                source={{ uri: image }}
                resizeMode="contain"
            />
        </View>
        <View style={styles.classTextContainer}>
            <Text style={styles.className}>{title}</Text>
            <Text style={styles.classCategory}>{categoryName}</Text>
            <Text style={styles.classProgress}>{progress}</Text>
        </View>
    </TouchableOpacity>
);

const ClassComponent = ({ loadingKelas, renderClassItem, kelas }) => {
    if (loadingKelas) {
        return <DarkLoadingTransparent/>
    } else if (kelas.length == 0){
        return <DarkNoDataTransparent/>
    }else {
        return (
            <FlatList
                style={styles.classListContainer}
                data={kelas}
                renderItem={renderClassItem}
                keyExtractor={(item) => item.id}
            />
        );
    }
};

const CategoryComponent = ({
    loadingKategori,
    kategori,
    fetchKelas,
    filter,
    setFilter,
}) => {
    if (loadingKategori) {
        return <LightLoadingTransparent/>
    } else {
        return (
            <Category
                kategori={kategori}
                fetchKelas={fetchKelas}
                filter={filter}
                setFilter={setFilter}
            />
        );
    }
};

export default function Purchased({ navigation }) {
    const [kategori, setKategori] = useState([]);
    const [kelas, setKelas] = useState([]);
    const [filter, setFilter] = useState("");
    const [loadingKategori, setLoadingKategori] = useState(true);
    const [loadingKelas, setLoadingKelas] = useState(true);
    const [user, setUser] = useContext(AuthContext);

    const fetchKelas = async (filter) => {
        try {
            setLoadingKelas(true);
            let tempKelas = [];
            let response;
            console.log("received: ", filter);

            if (filter != "") {
                response = await firebase
                    .firestore()
                    .collection("purchase")
                    .where("email", "==", user)
                    .where("categoryId", "==", filter)
                    .orderBy("purchaseDate", "desc")
                    .get();
            } else {
                response = await firebase
                    .firestore()
                    .collection("purchase")
                    .where("email", "==", user)
                    .orderBy("purchaseDate", "desc")
                    .get();
            }

            response.forEach((doc) => {
                tempKelas.push({
                    id: doc.id,
                    title: doc.data().title,
                    image: doc.data().image,
                    categoryId: doc.data().categoryId,
                    categoryName: doc.data().categoryName,
                    price: doc.data().price,
                    progress: doc.data().progress,
                    idKelas: doc.data().idKelas,
                });
            });
            setKelas(tempKelas);
            setLoadingKelas(false);
        } catch (err) {
            console.error(err);
            setLoadingKelas(false);
        }
    };

    useEffect(() => {
        const fetchKategori = async () => {
            try {
                setLoadingKategori(true);
                let tempKategori = [];

                const response = await firebase
                    .firestore()
                    .collection("category")
                    .orderBy("sort", "asc")
                    .get();

                response.forEach((doc) => {
                    tempKategori.push({
                        id: doc.id,
                        title: doc.data().title,
                        icon: doc.data().icon,
                    });
                });

                setKategori(tempKategori);
                setLoadingKategori(false);
            } catch (err) {
                console.error(err);
                setLoadingKategori(false);
            }
        };
        fetchKategori();
        return () => {
            console.log("clean up useEfect puchased page");
        };
    }, []);

    useFocusEffect(
        useCallback(() => {
            setFilter("");

            fetchKelas("");

            return () => {
                console.log("clean up useFocusEffect of Purchase Page");
            };
        }, [])
    );

    const renderClassItem = ({ item }) => (
        <ClassItem
            title={item.title}
            categoryName={item.categoryName}
            categoryId={item.categoryId}
            progress={item.progress}
            price={item.price}
            image={item.image}
            idKelas={item.idKelas}
            navigation={navigation}
            email={user}
        />
    );
    return (
        <View style={styles.container}>
            <View style={styles.upperContainer}>
                <View style={styles.smallLogoContainer}>
                    <Image
                        style={styles.smallLogo}
                        source={require("../assets/small-logo.png")}
                    />
                </View>
            </View>
            <CategoryComponent
                loadingKategori={loadingKategori}
                kategori={kategori}
                fetchKelas={fetchKelas}
                filter={filter}
                setFilter={setFilter}
            />
            <View style={styles.classSectionContainer}>
                <View style={styles.headerContainer}>
                    <Text style={styles.title}>
                        Ayo, lanjutkan pembelajaranmu!
                    </Text>
                </View>
                <ClassComponent
                    loadingKelas={loadingKelas}
                    renderClassItem={renderClassItem}
                    kelas={kelas}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        paddingTop: Constants.statusBarHeight,
        flex: 1,
        backgroundColor: primary1Color,
        justifyContent: "center",
        alignItems: "center",
    },
    upperContainer: {
        flexDirection: "row",
        height: 70,
        paddingHorizontal: 30,
        justifyContent: "center",
    },
    smallLogoContainer: {
        flex: 1,
        alignItems: "flex-end",
        justifyContent: "center",
    },
    smallLogo: {
        height: 30,
    },

    headerContainer: {
        justifyContent: "flex-end",
        alignSelf: "stretch",
        paddingHorizontal: 30,
        paddingTop: 30,
    },

    title: {
        marginVertical: 5,
        fontWeight: "700",
        fontSize: 26,
        // color: lightTextColor,
        color: darkTextColor,
    },

    classSectionContainer: {
        flex: 5,
        alignSelf: "stretch",
        paddingBottom: 10,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
        backgroundColor: primary6Color,
        marginTop: 20,
    },
    classListContainer: {
        marginHorizontal: 18,
    },
    classContainer: {
        margin: 6,
        padding: 5,
        borderRadius: 15,
        backgroundColor: lightTextColor,
        flex: 1,
        flexDirection: "row",
        elevation: 2
    },
    classImageContainer: {
        borderRadius: 10,
        marginRight: 5,
        flex: 1.5,
        justifyContent: "center",
    },
    classTextContainer: {
        marginBottom: 5,
        padding: 10,
        flex: 3.5,
        justifyContent: "space-evenly",
    },
    classImage: {
        width: "100%",
        height: 90,
    },
    className: {
        fontWeight: "700",
        fontSize: 14,
        color: darkTextColor,
    },
    classCategory: {
        fontSize: 12,
        color: darkTextColor,
    },
    classProgress: {
        fontWeight: "700",
        fontSize: 12,
        color: darkTextColor,
    },
});
