import React from "react";
import { Text, TextInput, StyleSheet, View } from "react-native";
import { darkTextColor, primary2Color } from "../constant/color";

export const Input = ({
    placeholder,
    secureTextEntry,
    autoCompleteType,
    onChangeText,
}) => {
    return (
        <View style={styles.inputGroup}>
            <TextInput
                style={styles.input}
                autoCompleteType={autoCompleteType}
                secureTextEntry={secureTextEntry}
                placeholder={placeholder}
                onChangeText={onChangeText}
            />
        </View>
    );
};

export const InputWithLabel = ({
    label,
    secureTextEntry,
    autoCompleteType,
    value,
}) => {
    return (
        <>
            <Text style={styles.inputLabel}>{label}</Text>
            <View style={styles.inputGroup}>
                <TextInput
                    style={styles.input}
                    autoCompleteType={autoCompleteType}
                    secureTextEntry={secureTextEntry}
                    editable={false}
                    value={value}
                />
            </View>
        </>
    );
};

const styles = StyleSheet.create({
    inputGroup: {
        flexDirection: "row",
    },
    input: {
        borderBottomWidth: 1,
        height: 45,
        borderColor: primary2Color,
        padding: 10,
        marginBottom: 10,
        marginTop: 2,
        flex: 1,
    },
    inputLabel: {
        fontWeight: "700",
        color: darkTextColor
    },
});
