import React, { useContext, useState } from "react";
import {
    Alert,
    Text,
    View,
    StyleSheet,
    Image,
} from "react-native";
import firebase from "firebase";
import { PrimaryButton } from "../components/Button";
import { Input } from "../components/Input";
import { loginUser } from "../data/firestoreHandler";
import { AuthContext } from "../context/AuthContext";
import {
    darkTextColor,
    lightTextColor,
    primary1Color,
    primary6Color,
} from "../constant/color";
import { LightLoadingScreen } from "../components/Placeholder"

export default function Login({ navigation }) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [loading, setLoading] = useState(false);
    const [user, setUser] = useContext(AuthContext);

    const submit = () => {
        setLoading(true);

        const auth = firebase
            .auth()
            .signInWithEmailAndPassword(email, password)
            .then(() => {
                const now = new Date();
                const data = {
                    email,
                    lastLogin: now,
                };
                loginUser(data);
                setUser(data.email);
                navigation.navigate("MainApp");
            })
            .catch((error) => {
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log(errorCode.toString());
                console.log(errorMessage.toString());
                Alert.alert("Coba lagi", "Data belum sesuai, ayo coba lagi!");
                setLoading(false);
            });

        return () => auth();
    };

    if (loading) {
        return (
            <LightLoadingScreen />
        );
    } else {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image
                        style={styles.logo}
                        source={require("../assets/white.png")}
                        resizeMode="contain"
                    />
                </View>
                <View style={styles.form}>
                    <Text style={styles.title}>Masuk</Text>
                    <View style={styles.content}>
                        <Input
                            placeholder="Email"
                            autoCompleteType={"email"}
                            value={email}
                            onChangeText={(value) =>
                                setEmail(value.toLowerCase())
                            }
                        />
                        <Input
                            placeholder="Kata sandi"
                            secureTextEntry={true}
                            value={password}
                            onChangeText={(value) => setPassword(value)}
                        />
                    </View>
                    <View style={styles.buttonSection}>
                        <PrimaryButton text="Masuk" onPress={submit} />
                    </View>
                    <View style={styles.bottomSection}>
                        <Text style={styles.credits}>
                            Dibuat oleh:
                        </Text>
                        <Text style={styles.credits}>
                            Marcelina Anggraeni
                        </Text>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: primary1Color,
    },
    header: {
        flex: 1,
        justifyContent: "center",
        alignSelf: "stretch",
        paddingHorizontal: 60,
    },
    form: {
        flex: 1.75,
        justifyContent: "center",
        alignSelf: "stretch",
        paddingHorizontal: 60,
        paddingVertical: 30,
        backgroundColor: primary6Color,
        borderTopStartRadius: 50,
        borderTopEndRadius: 50,
    },
    content: {
        paddingVertical: 10,
        justifyContent: "center",
        alignItems: "center",
    },
    buttonSection: {
        flex: 1,
        justifyContent: "flex-start",
        alignItems: "center",
        marginHorizontal: 30,
        marginTop: 20
    },
    bottomSection: {
        height: 50,
        justifyContent: "center",
        alignSelf: "stretch",
        alignItems: "center",
    },
    logo: {
        height: 70,
        width: 70,
        alignSelf: "center",
    },

    title: {
        marginTop: 10,
        marginBottom: 15,
        fontWeight: "700",
        fontSize: 26,
        color: darkTextColor,
    },
    subtitle: {
        marginTop: 10,
        fontSize: 14,
        color: darkTextColor,
    },
    appName: {
        marginTop: 10,
        marginBottom: 15,
        fontWeight: "700",
        fontSize: 20,
        color: lightTextColor,
        textAlign: "center",
    },
    credits: {
        color: darkTextColor,
        opacity: 0.5
    },
});
