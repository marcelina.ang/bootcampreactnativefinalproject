import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import Ionicons from "react-native-vector-icons/Ionicons";

import Account from "../pages/Account";
import Course from "../pages/Course";
import CoursePreview from "../pages/CoursePreview";
import Home from "../pages/Home";
import Login from "../pages/Login";
import Onboarding from "../pages/Onboarding";
import Purchased from "../pages/Purchased";
import Register from "../pages/Register";

import { AuthProvider } from "../context/AuthContext";
import {
    disabledColor,
    lightTextColor,
    primary1Color,
    primary6Color,
} from "../constant/color";

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => (
    <Tab.Navigator
        initialRouteName="Beranda"
        screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
                let iconName;

                if (route.name === "Beranda") {
                    iconName = focused ? "home" : "home-outline";
                } else if (route.name === "Daftar Kelasku") {
                    iconName = focused ? "book" : "book-outline";
                } else if (route.name === "Akun") {
                    iconName = focused ? "person" : "person-outline";
                }

                return <Ionicons name={iconName} size={size} color={color} />;
            },
            tabBarActiveTintColor: primary1Color,
            tabBarInactiveTintColor: disabledColor,
            tabBarStyle: {height: 50, paddingVertical: 5}
        })}
    >
    <Tab.Screen
        name="Beranda"
        component={Home}
        options={{ headerShown: false }}
    />
        <Tab.Screen
            options={{ headerShown: false }}
            name="Daftar Kelasku"
            component={Purchased}
        />
        <Tab.Screen
            name="Akun"
            component={Account}
            options={{ headerShown: false }}
        />
    </Tab.Navigator>
);

export default function Router() {
    return (
        <AuthProvider>
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Onboarding">
                    <Stack.Screen
                        name="Course"
                        component={Course}
                        options={{
                            headerTitle: "",
                            headerTintColor: primary6Color,
                            headerStyle: {
                                elevation: 0,
                                backgroundColor: primary1Color,
                            },
                            headerLeftContainerStyle: {
                                paddingHorizontal: 10,
                                alignSelf: "center",
                                paddingVertical: 30,
                            },
                        }}
                    />
                    <Stack.Screen
                        name="CoursePreview"
                        component={CoursePreview}
                        options={{
                            headerTitle: "",
                            headerTintColor: primary6Color,
                            headerStyle: {
                                elevation: 0,
                                backgroundColor: primary1Color,
                            },
                            headerLeftContainerStyle: {
                                paddingHorizontal: 10,
                                alignSelf: "center",
                                paddingVertical: 30,
                            },
                        }}
                    />
                    <Stack.Screen
                        name="Login"
                        component={Login}
                        options={{
                            headerTitle: "",
                            headerTintColor: lightTextColor,
                            headerStyle: {
                                elevation: 0,
                                backgroundColor: primary1Color,
                            },
                            headerLeftContainerStyle: {
                                paddingHorizontal: 10,
                                alignSelf: "center",
                                marginTop: 30,
                            },
                        }}
                    />
                    <Stack.Screen
                        name="Onboarding"
                        component={Onboarding}
                        options={{ headerShown: false }}
                    />

                    <Stack.Screen
                        name="MainApp"
                        component={MainApp}
                        options={{ headerShown: false }}
                    />
                    <Stack.Screen
                        name="Register"
                        component={Register}
                        options={{
                            headerTitle: "",
                            headerTintColor: lightTextColor,
                            headerStyle: {
                                elevation: 0,
                                backgroundColor: primary1Color,
                            },
                            headerLeftContainerStyle: {
                                paddingHorizontal: 10,
                                alignSelf: "center",
                                marginTop: 30,
                            },
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </AuthProvider>
    );
}
